# 2015-11-09-XAML

### 1.Boomstructuur
>Wat wordt bedoeld met "XAML heeft een boomstructuur net zoals HTML" ?

##### Antwoord:
Dat zowel bij HTML-code als bij de XAML-code de codes van elkaar overerven en in elkaar nestelen.

### 2.Complexe UI-objecten
>Wat wordt bedoeld met "Met XAML kunnen complexe UI-objecten (of Controls) gemaakt worden doordat ze in elkaar kunnen genest worden?" Waarom is het hiervoor soms van belang om de zogenaamde Property Element Syntax te gebruiken?

##### Antwoord:
Als je de content van een element andere elmenten wilt laten bevatten moet je wel de property syntax gebruiken.

      <Button x:Name="button" Height="98">
              <Button.Content>
                  <StackPanel>
                      <TextBlock>Hallo</TextBlock>
                      <TextBlock>Wereld</TextBlock>
                  </StackPanel>
              </Button.Content>
          </Button>



### 3.Koppelen event-handler
>Welke 2 manieren zijn er in om events te koppelen aan een event-handler? Geef ook de method-signatuur (d.w.z. parameters/return-waarde) van min. 2 events: Click en MouseMove. Leg uit waar het verschil in method-signatuur tussen deze 2 events vandaan komt.

##### Antwoord:
Je hebt 2 verschillende argumenten omdat de objecten anders zijn. Deze objecten hebben hun eigen relevante informatie nodig

      private void Button_Click(object sender, RoutedEventArgs e)
              {
                 halloLabel.Content = "EVENTS!!!";
                 halloLabel.MouseDoubleClick += HalloLabel_MouseDoubleClick;
                 halloLabel.Background = Brushes.Aqua;
              }

              private void button_TouchEnter(object sender, TouchEventArgs e)
                     {

                     }

### 4.Propreties Window-class
>Wat doen de volgende properties v.d. Window-class? Onderzoek in VS of/en zoek op WPFtutorial.com. Probeer eerst a.h.v. de naam v.d. property te gokken wat elke property doet.

>Icon
ResizeMode
ShowInTaskbar
SizeToContent
Topmost
WindowStartupLocation
WindowState
Geef 2 code-fragmenten waar je een property gebruikt.

##### Antwoord:

****Icon:****
****ResizeMode: ****
****ShowInTaskbar: ****
****SizeToContent: ****
****Topmost: ****
****WindowStartupLocation: ****
****WindowState: ****


### 5.Tweede venster
>Hoe kan je een tweede venster aan een applicatie toevoegen en ervoor zorgen dat die getoond wordt, b.v. bij een click op een knop?

##### Antwoord:
Gewoon een nieuw object maken.

### 6.Belangrijkste configuratie
>Wat is misschien wel het belangrijkste dat je kan configureren in App.Xaml?

##### Antwoord:
Je resources

### 7.Resources
>Leg uit wat resources zijn a.h.v. de Nederlandse vertaling. Welke types van resources kan je bedenken voor een desktop-applicatie in het algemeen of een WPF-applicatie in het bijzonder? Hoe voeg je ze in WPF toe aan:

>een Window?
de gehele applicatie?

##### Antwoord:
Resources zijn bestanden, toepassingen of variabelen die je kan gebruiken in je programma. Met XAML kan je z heel efficient gebruiken en kan je ze toewijzen aan enkele elementen op je window.
